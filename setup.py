#!/usr/bin/env python

from setuptools import find_packages, setup

setup(
    name="atd2020",
    version="0.1.2",
    description="Baseline detector, metrics, and utilities for ATD2020 challenge.",
    author="Douglas Mercer",
    author_email="dgm161@arl.psu.edu",
    python_requires=">=3.8",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "arrow",
        "ipykernel",
        "nb_black",
        "notebook",
        "numpy>=1.16.6",
        "matplotlib>=3.1.3",
        "pandas==1.0.5",  # See test_pandas_unsorted_datetime_index_indexed_by_string
        "pyarrow>=0.16.0",
        "seaborn",
        "scikit-learn",
        "statsmodels",
    ],
    extras_require={
        "dev": [
            "black>=20.8b1",
            "check-manifest>=0.44",
            "flake8>=3.8.3",
            "ipykernel>=5.3.4",
            "interrogate>=1.3.1",
            "isort>=5.6.4",
            "mypy>=0.782",
            "nb-black>=1.0.7",
            "nb-clean>=1.6.0",
            "notebook>=6.1.4",
            "pydocstyle>=5.1.1",
            "pyinstrument>=0.10.1",
            "pytest>=6.0.2",
            "pytest-cov>=2.10.1",
            "pytest-html>=2.1.1",
            "pytest-xdist>=2.1.0",
            "requests-mock>=1.8.0",
            "sphinx>=3.2.1",
            "sphinx-rtd-theme>=0.5.0",
        ],
    },
)
