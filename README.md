# atd2020
[![License](
    https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](
    https://opensource.org/licenses/BSD-3-Clause)
[![platform](
    https://img.shields.io/badge/platform-linux--64%20|%20win--64%20|%20osx--64-blue.svg)](
    https://img.shields.io/badge/platform-linux--64%20|%20win--64%20|%20osx--64-blue)
[![python](
    https://img.shields.io/badge/python-3.8-blue.svg)](
    https://img.shields.io/badge/python-3.8-blue)
[![Code style: black](
    https://img.shields.io/badge/code%20style-black-000000.svg)](
    https://github.com/psf/black)
[![Pipeline](
    https://gitlab.com/algorithms-for-threat-detection/2020/atd2020/badges/develop/pipeline.svg)](
    https://gitlab.com/algorithms-for-threat-detection/2020/atd2020/pipelines)
[![coverage report](
    https://gitlab.com/algorithms-for-threat-detection/2020/atd2020/badges/develop/coverage.svg)](
    https://algorithms-for-threat-detection.gitlab.io/2020/atd2020/cov/)
[![Gitter](
    https://badges.gitter.im/atd2020/atd2020.svg)](
    https://gitter.im/atd2020/atd2020?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

Welcome to the ATD2020 challenge!

In this README, we begin by defining this year's [challenge problem](#problem-description) and [dataset](#about-the-data). We continue by providing detailed instructions on how to [get started](#getting-started) by configuring your development environment and acquiring the `atd2020` utility code and dataset. Finally, we describe how to create [Gitlab Issues](#how-to-submit-an-issue-on-gitlab) generally, and we specifically point you to two issue boards where you can ask [questions about the dataset/utility code](#how-to-ask-questions) and submit your [anomaly classification results](#how-to-submit-solutions) to the ATD2020 administrative team to compute metrics on the holdout dataset.

## Problem Description
The goal of this year's challenge is to find anomalous observations in sparsely sampled traffic flow data. We define an anomaly to be a traffic flow observation more than three standard deviations away from the mean flow for a particular sensor, on a particular weekday (e.g., Wednesday), and within a particular hour (12-1pm).

The dataset you've been provided is incomplete. Sensors in the provided dataset were split equally between 5 sample rates (1%, 2%, 5%, 10%, and 20%), and each sensor contains only a fraction of observations proportional to its sample rate. However, the complete dataset was used to determine which observations were labelled as anomalies.

Your challenge is to create a model that performs well even at low sampling rates. Submissions will be judged by the macro (unweighted) average of the F<sub>1</sub> scores for anomalous observations across all `Fraction_Observed` values.

## Repository contents

You can find sphinx documentation for the `atd2020` library at [https://algorithms-for-threat-detection.gitlab.io/2020/atd2020/docs/](https://algorithms-for-threat-detection.gitlab.io/2020/atd2020/docs/).

* `atd2020/` - a python module with utilities you may find useful.
    * `detector.py` - contains a BaselineDetector class, which was used to label anomalies in the full dataset and will serve as a baseline to compare your detector's performance against.
    * `detrend.py` - contains functions used to detrend data from `data/` to encourage the time series to be [stationary](https://www.itl.nist.gov/div898/handbook/pmc/section4/pmc442.htm).
    * `metrics.py` - contains functions used to compute classification metrics.
    * `utilities.py` - contains functions for reading data from `data/` into a `pandas.DataFrame` and writing [ATD compliant anomaly .csv's](#anomalies-csv-format)
* `data/` - a directory containing the challenge datasets.
    * `City1.parquet.brotli` - A complete, ground truthed dataset for your development, testing, and evaluation.
    * `City2_downsampled.parquet.brotli` - A downsampled, unlabelled dataset for the final evaluation of your detector's performance.
* `results/City1.csv` - [Example output](#anomalies-csv-format) for the `City1` dataset.
* `Challenge.ipynb` - a walkthrough illustrating how to read in and view the data, find anomalies using a baseline detector, and compute metrics (if using dataset `data/City1.parquet.brotli`, the full dataset provided.).
* `LICENSE` - The license for the repository and its contents.
* `Metrics.ipynb` - a notebook for computing metrics from [an anomaly .csv](#anomalies-csv-format)
* `metrics.sh` - a shell script for computing metrics from [an anomaly .csv](#anomalies-csv-format)
* `environment.yaml` - Environment file used to install the `atd2020` dependencies.
* `setup.py` - Setup script used to install the `atd2020` module.


In addition, there are several files related to the development, testing, and CI/CD of the repository. You can ignore the following files:

* `.coveragerc`
* `.gitattributes`
* `.gitignore`
* `.gitlab-ci.yml`
* `dev.yaml`
* `pytest.ini`


## About the data

This dataset was obtained from the California transportation website [Caltrans PEMS](http://pems.dot.ca.gov). The data we've provided is only a small subset of the complete dataset, spanning only a subset of time, selecting only a subset of stations, and providing only a subset of the available fields. Moreover, we have taken some steps to make the absolute position of the sensors ambiguous by normalizing the `Latitude` and `Longitude` of the sensors between 0 and 1 and remapping the station IDs.

## Getting started

### Outline

1. [Install tools](#install-tools)
1. (Optional) [Make Gitlab authentication convenient](#optional-make-gitlab-authentication-convenient)
1. [Clone atd2020](#clone-atd2020)
1. [Install atd2020](#install-atd2020)
1. [Open Challenge.ipynb](#open-challenge-notebook)

### Install tools

Install all of the following software:

* [`git`](https://git-scm.com/downloads)
    * Note: if you are on Mac or Linux, you probably already have `git` installed.
* [`git-lfs`](https://git-lfs.github.com/)
    * Download `git-lfs` from the download url specified on the above page
    * Run `git lfs install`
    * The remainder of steps are not relevant to challenge participants
* [Anaconda Python](https://www.anaconda.com/distribution/)

Note for people who already using conda/virtual environments: these tools should be accessible from a virtual environment that we will create in a later step. We recommend installing these tools at either the system level, user level, or to your base virtual environment.


### (Optional) Make Gitlab authentication convenient

Because we are working in a private repository, Gitlab requires that you authenticate as your Gitlab user when performing server-side git commands (e.g., `clone`, `fetch`, `pull`).

We recommend that you utilize one of the following approaches to make authenticating to Gitlab more convenient.

1. ssh key-pair
	- Recommended if your local network's firewall allows ssh-tunneling to Gitlab
	- [Create an ssh key](https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair)
	- [Upload it to your Gitlab user](https://gitlab.com/profile/keys)
1. [Git Credentials Manager](https://git-scm.com/book/en/v2/Git-Tools-Credential-Storage)
	- Recommend if your local network's firewall forbids ssh-tunneling
	- If you use `git config --global credential.helper cache`, git will temporarily cache your username and password.
	- If you use `git config --global credential.helper store` git will permanently store your username and password in a plaintext file in your home directory
	- We recommend using whichever option fits your security requirements

If you do not use either of these approaches, then you will need to authenticate (by entering your username and password into your terminal) before every server-side command.

### Clone atd2020

If in [the previous section](#optional-make-gitlab-authentication-convenient) you elected to setup ssh-based authentication, run

```
git clone git@gitlab.com:algorithms-for-threat-detection/2020/atd2020.git
```

Otherwise, you can clone using

```
git clone https://gitlab.com/algorithms-for-threat-detection/2020/atd2020.git
```

If using the `https` address, you will need to enter your Gitlab username and password when prompted.

Once you clone `atd2020`, run `cd atd2020` to change directory into the `atd2020/` repository folder.


### Install atd2020

This section assumes you are using [conda](https://anaconda.org/anaconda/conda) to manage environments and packages.

In the previous section, you should have `git clone`d the `atd2020` repository and `cd`'d into the `atd2020/` repository folder.

To install all dependencies for `atd2020`, in your anaconda sourced terminal and from within the `atd2020/` repository folder do

```
conda env create -f environment.yaml
```

After, to be placed in a terminal environment with all prerequisites installed, run:

```
conda activate atd2020
```

Next, to install the `atd2020` helper functions, do:

```
pip install .
```

Finally, create a kernel based on your atd2020 environment by doing the following (with the `atd2020` conda environment active)
```
python -m ipykernel install --name atd2020 --user
```

This final step will allow you to use the `atd2020` conda environment software from within Jupyter notebooks using the `atd2020` kernel.


### Open Challenge Notebook

Once `atd2020` has been installed and you've created a conda kernel named `atd2020`, you are ready to open `Challenge.ipynb` in your Jupyter Notebook server.

You can create a local Jupyter server by either using the Anaconda Navigator GUI or entering `jupyter notebook` into a conda-sourced terminal.

Please refer to the [Jupyter Notebook documentation](https://jupyter-notebook.readthedocs.io/en/stable/notebook.html#notebook-user-interface) if you are unfamiliar with the Jupyter Notebook user interface.

## Group chat on atd2020 Gitter

You can use [![Gitter](https://badges.gitter.im/atd2020/atd2020.svg)](https://gitter.im/atd2020/atd2020?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge) to chat with other participants and the ATD2020 administrative team in a persistent chat. Be sure to log-in via Gitlab to ensure you have permission to join the room.

## How to ask questions

You may use the [ATD2020 Issue Board](https://gitlab.com/algorithms-for-threat-detection/2020/atd2020/-/issues) to ask questions about the dataset and utility code. Please refer to the [section in the FAQ](#how-to-submit-an-issue-on-gitlab) for general instructions on how to submit issues.

On this board, all ATD2020 participants and the ATD2020 administrative team have access to **all issues** (even confidential issues). So, assume that all participants will be able to read your issue on this board.

We encourage participants to collaboratively answer any questions posted to this issue board, though the ATD2020 administrative team will do our best to monitor this board and answer questions in a timely manner.

## How to submit solutions

|Deadline         | Date            |
| --------------- | --------------- |
|City2 Submission |September 1, 2020|
|City3 Submission |October 1, 2020|


At each of the deadlines, you will submit your solution for evaluation on the holdout dataset (City2 and City3 for the mid-point and final deadlines, respectively). We will utilize **confidential issues** on the [atd2020submissions Issue Board](https://gitlab.com/algorithms-for-threat-detection/2020/atd2020submissions/-/boards) for this purpose. Please refer to the [section in the FAQ](#how-to-submit-an-issue-on-gitlab) for general instructions on how to submit issues. On the [atd2020submissions Issue Board](https://gitlab.com/algorithms-for-threat-detection/2020/atd2020submissions/-/boards), only the ATD2020 administrative team will have access to **confidential issues**. Be sure to mark your issue as confidential when submitting your solution.

City2 solutions must be submitted to the [atd2020submissions Issue Board](https://gitlab.com/algorithms-for-threat-detection/2020/atd2020submissions/-/boards) no later than **September 1, 2020**. Similarly, City3 solutions must be submitted to the [atd2020submissions Issue Board](https://gitlab.com/algorithms-for-threat-detection/2020/atd2020submissions/-/boards) no later than **October 1, 2020**.

Please use the following workflow when submitting your solution:

1. Generate an [ATD2020 challenge compliant anomalies.csv](#anomalies-csv-format) for both the City1 and City\# (City2 or City3, depending on the deadline) datasets.
1. Ensure you can successfully compute metrics for the City1 dataset using either of the metrics workflows described in [metrics workflows](#metrics-workflows).
1. Compress your `City#.csv` into a `.zip` file.
1. Visit the [atd2020submissions issue board](https://gitlab.com/algorithms-for-threat-detection/2020/atd2020submissions/-/boards).
1. Create a confidential issue, titled "YourName - City\# Submission".
1. Attach `City#zip`, your zipped CSV of anomaly predictions for the City \# dataset, to your issue.
1. Assign the issue to @dougmercer.
1. Wait for the ATD2020 administrative team to respond. We will attempt to compute metrics for your submission, and will reply to the issue with either your metric scores or will begin a dialog with you to help resolve any issues with your submission.

### Anomalies `.csv` format

Valid submissions will be `.csv` files of the following format:

* The first row is a header row (i.e., indicate which column corresponds to which variable).
* Contains the following columns
    * `Timestamp` (format: `"%m/%d/%Y %H:%M:%S"`)
    * `ID`
    * `Anomaly`
* Columns are separated by commas, "`,`".

You can use `atd2020.utilities.to_csv(df[["Timestamp", "ID", "Anomaly"]])` to produce a compliant CSV from a pandas DataFrame that contains the appropriate fields.

Please refer to `results/City1.csv` for an example results file.

### Metrics workflows

#### Metrics.ipynb

You can compute metrics using `Metrics.ipynb`. To do this, open the notebook and modify the filepaths as necessary to load your anomalies `.csv` data.

#### Command-line interface to atd2020.metrics

You can run `atd2020.metrics` to compute metrics from the command line. To compute the overall metrics for the provided `results/City1.csv`, do:

```
python -m atd2020.metrics -p results/City1.csv -t data/City1.parquet.brotli
```

To also compute metrics for subsets of the observations grouped by `Fraction_Observed`, do:

```
python -m atd2020.metrics -p results/City1.csv -t data/City1.parquet.brotli -g Fraction_Observed
```

To view help for this command-line interface, do:

```
python -m atd2020.metrics -h
```

## FAQ

### How to submit an Issue on Gitlab

Gitlab provides an issue board you can use to submit questions or request improvements. Please refer to [these instructions](https://docs.gitlab.com/ee/user/project/issues/) to learn how to submit issues in Gitlab.


### The `atd2020` codebase on Gitlab has updated since I first cloned the repository. How can I get the changes?

We can do this in five, high level steps.

1. Ensure your working directory is clean.
  - Verify that there are no `staged` or `unstaged` changes when running `git status` (untracked are probably OK).
1. Pull the changes from Gitlab into your current branch in your local repository
  - `git pull --rebase origin develop`
  - Note: If you have made commits to any files originally in `atd2020`, there is a chance that there will be conflicts. Please refer to online references for guidance on how to resolve git merge conflicts (e.g., https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/resolving-a-merge-conflict-using-the-command-line)
1. Reinstall the `atd2020` conda environment
  - If Windows, be sure to do the following first to avoid the Windows filesystem locking files that require re-installation:
    - `conda deactivate` to the base environment
    - Close all applications/tasks (besides your current conda prompt) that may have a lock on any file in your `atd2020` conda environment
      - `jupyter-notebook` tasks, `jupyter notebook` browser windows, windows explorer open to the `atd2020` conda environment directory, etc.
  - From the `atd2020` repository folder, run `conda env create -f environment.yaml --force`
  - `conda activate atd2020`
1. Reinstall the `atd2020` python package into the conda environment
  - Same as [the above installation section](#install-atd2020)
1. Recreate the `atd2020` jupyter notebook kernel
  - Same as [the above installation section](#install-atd2020)


### `ArrowInvalid: Parquet magic bytes not found in footer. Either the file is corrupted or this is not a parquet file`

It's likely that the immediate source of your problem is that the `City*.parquet.brotli` file you are trying to read is a small, plain-text placeholder file and not the real data file. This occurs if you cloned the repository (`git clone ...`) *before* correctly configuring `git-lfs` (`git lfs install`).

To verify that the problematic file is a `git-lfs` placeholder file, open the file in a text editor and check that the contents are a small text file.

One way of solving this is to delete the `atd2020` repository directory, run `git lfs install`, and then re-clone the `atd2020` repository.

However, the more direct way of solving the problem is to re-run `git lfs install` and then run `git lfs pull` from within the `atd2020` repository directory. If successful, this command will take a few moments to download the data file from Gitlab and replace all the placeholder files with the real data files.

### `ImportError: Unable to find a usable engine; tried using: 'pyarrow', 'fastparquet'. pyarrow or fastparquet is required for parquet support`

We hope that no one runs into this error, as we tested the `environment.yaml` on Linux, Windows, and Mac environments. However, if you do, it means that `conda` installed incompatible versions of `pyarrow`, `arrow-cpp`, and potentially other dependencies in your environment.

We recommend submitting an issue to the [atd2020 issue board](#how-to-submit-an-issue-on-gitlab) and we'll work with you to resolve the problem. Please include the output of `conda list` and details about your development environment (operating system and distribution/version of anaconda).

### How does my Macro Average F1 score compare to other participants? Is there a Leaderboard?

Below, we include anonymized results for each dataset. Submissions are denoted by the Gitlab Issue # used to submit the solutions.

Results will be added occassionaly (as more teams make their submissions).

#### City2

|   Place | Gitlab Issue #                           |   F1 MacroAvg |
|--------:|:-----------------------------------------|--------------:|
|       1 | 6                                        |     0.551284  |
|       2 | 9                                        |     0.536932  |
|       3 | 7                                        |     0.486214  |
|       4 | 5                                        |     0.479521  |
|       5 | 11                                       |     0.453511  |
|       6 | Baseline(('ID', 'Hour'), 'detrended', 2) |     0.363013  |
|       7 | Baseline(('ID', 'Hour'), 'raw', 2)       |     0.355137  |
|       8 | Baseline(('ID', 'Hour'), 'detrended', 3) |     0.340417  |
|       9 | Baseline(('ID', 'Hour'), 'raw', 3)       |     0.3313    |
|      10 | 12                                       |     0.0184293 |

![](docs/leaderboard.png)

### City3

|   Place | Gitlab Issue #                           |   F1 MacroAvg |
|--------:|:-----------------------------------------|--------------:|
|       1 | 20                                       |      0.553277 |
|       2 | 13                                       |      0.540105 |
|       3 | 19                                       |      0.530534 |
|       4 | 17                                       |      0.485046 |
|       5 | 18                                       |      0.478363 |
|       6 | 21                                       |      0.35995  |
|       7 | Baseline(('ID', 'Hour'), 'detrended', 3) |      0.345448 |
|       8 | Baseline(('ID', 'Hour'), 'detrended', 2) |      0.337026 |
|       9 | Baseline(('ID', 'Hour'), 'raw', 3)       |      0.334864 |
|      10 | Baseline(('ID', 'Hour'), 'raw', 2)       |      0.326645 |

![](docs/leaderboard_city3.png)

*Last updated 10/05/2020 at 1:01 PM UTC.*

# We hope you enjoy the ATD 2020 Challenge!
