.. ATD2020 documentation master file, created by
   sphinx-quickstart on Fri Feb 12 17:19:30 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ATD2020's documentation!
===================================

This repository provides a baseline anomaly detector, linear detrending module, metrics, and some supporting I/O utilities for the ATD2020 challenge.

You can find documentation for the ATD2021 code base `here <https://algorithms-for-threat-detection.gitlab.io/2020/atd2020/docs/>`_.

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
