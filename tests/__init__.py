from . import (
    test_black,
    test_detector,
    test_detrend,
    test_metrics,
    test_regressions,
    test_utilities,
)
