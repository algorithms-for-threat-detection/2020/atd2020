import unittest

import numpy as np
import pandas as pd
import scipy

import atd2020


def make_data(n_obs=100, uids=(0, 1), start="2020/1/1 00:00:00"):
    ids = np.repeat(uids, n_obs)
    timestamp_id = pd.date_range(start, periods=n_obs, freq="h").to_list()
    timestamp = np.tile(timestamp_id, len(uids))
    starts = np.random.randint(0, 100, len(uids))
    ends = np.random.randint(200, 300, len(uids))
    total_flow = np.hstack([np.linspace(s, e, n_obs) for s, e in zip(starts, ends)])
    df = pd.DataFrame({"ID": ids, "Timestamp": timestamp, "TotalFlow": total_flow})
    return pd.concat(
        [group.sample(frac=0.1) for _, group in df.groupby("ID")]
    ).reset_index(drop=True)


def compute_slope(df, ycol="TotalFlow"):
    x = (df["Timestamp"] - df["Timestamp"].min()).dt.total_seconds()
    y = df[ycol]
    return scipy.stats.linregress(x, y)[0]


def compute_slopes(df):
    return [compute_slope(df) for _, df in df.groupby("ID")]


class TestDetrend(unittest.TestCase):
    def test_mean_preserved(self):
        df = make_data()
        expected = df.groupby("ID")["TotalFlow"].mean()
        actual = atd2020.detrend.detrend(df).groupby("ID")["TotalFlow"].mean()
        np.testing.assert_allclose(expected, actual)

    def test_idempotent(self):
        df = make_data()
        df_one = atd2020.detrend.detrend(df)
        df_two = atd2020.detrend.detrend(df_one)
        pd.testing.assert_series_equal(df_one["TotalFlow"], df_two["TotalFlow"])

    def test_copies_original(self):
        df = make_data()
        df_detrended = atd2020.detrend.detrend(df)
        pd.testing.assert_series_equal(
            df["TotalFlow"], df_detrended["TotalFlow_original"], check_names=False
        )

    def test_trend_0(self):
        df = make_data()
        df_detrended = atd2020.detrend.detrend(df)
        slopes = compute_slopes(df_detrended)
        np.testing.assert_allclose(slopes, 0, atol=1e-8)

    def test_detrend_inplace_false(self):
        df = make_data()
        df_detrended = atd2020.detrend.detrend(df, inplace=False)
        self.assertIsNotNone(df_detrended)
        self.assertIn("TotalFlow_original", df_detrended.columns)
        self.assertNotIn("TotalFlow_original", df.columns)

    def test_detrend_inplace_true(self):
        df = make_data()
        should_be_none = atd2020.detrend.detrend(df, inplace=True)
        self.assertIsNone(should_be_none)
        self.assertIn("TotalFlow_original", df.columns)

    def test_detrend_one_id(self):
        df = make_data(uids=(0,))
        _ = atd2020.detrend.detrend(df, inplace=True)
        self.assertTrue(True, "Just checking to see if this errors")


if __name__ == "__main__":
    unittest.main(
        testRunner=xmlrunner.XMLTestRunner(output="test-reports"),
        failfast=False,
        buffer=False,
        catchbreak=False,
    )
