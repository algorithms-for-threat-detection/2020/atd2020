import glob
import json
import sys
import unittest

import black


def is_black(x):
    x_formatted = black.format_str(x, mode=black.FileMode())
    if not x.endswith("\n"):
        # Remove trailing newline
        x_formatted = x_formatted[:-1]
    return x == x_formatted


def is_black_cell(cell):
    placeholder = '"placeholder"\n'
    cell_no_magic = [line if not line.startswith("%") else placeholder for line in cell]
    cell_no_magic_as_str = "".join(cell_no_magic)
    return is_black(cell_no_magic_as_str)


def is_black_notebook(fpath):
    with open(fpath) as f:
        notebook = json.load(f)
    cells = [c["source"] for c in notebook["cells"] if c["cell_type"] == "code"]
    return all([is_black_cell(c) for c in cells])


def get_notebooks(directory):
    return glob.glob("**/*.ipynb", recursive=True)


class TestNotebooksBlack(unittest.TestCase):
    def test_challenge_notebook_is_black(self):
        notebook = "Challenge.ipynb"
        self.assertTrue(is_black_notebook(notebook), f"{notebook} is not black.")

    def test_metrics_notebook_is_black(self):
        notebook = "Metrics.ipynb"
        self.assertTrue(is_black_notebook(notebook), f"{notebook} is not black.")


if __name__ == "__main__":
    unittest.main(
        testRunner=xmlrunner.XMLTestRunner(output="test-reports"),
        failfast=False,
        buffer=False,
        catchbreak=False,
    )
