import unittest

import pandas as pd


class TestRegressions(unittest.TestCase):
    def test_pandas_unsorted_datetime_index_indexed_by_string(self):
        pd.DataFrame(index=pd.to_datetime(["2016-01", "2016-03", "2016-02"]))[
            "2016-01":"2016-02"
        ]
        try:
            pd.DataFrame(index=pd.to_datetime(["2016-01", "2016-03", "2016-02"]))[
                "2016-01":"2016-02"
            ]
        except AssertionError as e:
            print(e)
            self.assertFalse(isinstance(e, AssertionError))
            raise e
