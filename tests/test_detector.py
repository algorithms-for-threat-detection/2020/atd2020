import unittest

import matplotlib as mpl
import numpy as np
import pandas as pd
from numpy.testing import assert_array_equal
from pandas.testing import assert_frame_equal

import atd2020
from atd2020.detector import BaselineDetector


def make_data(index, start="2020/1/1 00:00:00"):
    mu = np.random.randint(0, 20, len(index))
    sigma = np.random.randint(1, 3, len(index))
    expected_fit = pd.DataFrame(index=index, data={"mean": mu, "std": sigma})

    n = 100000
    timestamp = pd.date_range(start, periods=n, freq="h").to_list()
    data = np.random.normal(loc=mu, scale=sigma, size=(n, len(mu)))
    if isinstance(index, pd.MultiIndex):
        prepare_index = lambda x: pd.MultiIndex.from_tuples([x])
    else:
        prepare_index = lambda x: [x]
    df = pd.concat(
        [
            pd.DataFrame(
                {"Timestamp": [timestamp], "TotalFlow": [col]},
                index=prepare_index(ind),
            )
            .rename_axis(get_names(index))
            .apply(pd.Series.explode)
            .convert_dtypes()
            for ind, col in zip(index, data.T)
        ]
    )
    return df, expected_fit


def make_anomalies(df_fit):
    return (
        pd.concat(
            [
                df_fit["mean"] + np.random.uniform(-8, -4) * df_fit["std"],
                df_fit["mean"] + np.random.uniform(4, 8) * df_fit["std"],
            ]
        )
        .to_frame(name="TotalFlow")
        .rename_axis(get_names(df_fit.index))
        .reset_index()
    )


def make_normal(df_fit):
    return (
        pd.concat(
            [
                df_fit["mean"] + np.random.uniform(-df_fit["std"], 0),
                df_fit["mean"] + np.random.uniform(0, df_fit["std"]),
            ]
        )
        .to_frame(name="TotalFlow")
        .rename_axis(get_names(df_fit.index))
        .reset_index()
    )


def make_df_test_and_expected(expected_fit):
    df_normal = make_normal(expected_fit)
    df_anomalies = make_anomalies(expected_fit)
    df_test = pd.concat([df_normal, df_anomalies])

    expected = np.repeat([False, True], [len(df_normal), len(df_anomalies)])
    return df_test, expected


def get_names(index):
    return index.names if isinstance(index, pd.MultiIndex) else index.name


class TestMetrics(unittest.TestCase):
    def test_fit(self):
        ids = [1, 2, 3]
        index = pd.Index(ids, name="ID")
        df_train, expected = make_data(index)

        actual = (
            BaselineDetector(groupby=["ID"]).fit(df_train).group_stats[["mean", "std"]]
        )

        assert_frame_equal(
            actual.round(),
            expected,
            check_dtype=False,
            check_less_precise=1,
        )

    def test_predict(self):
        ids = [1, 2, 3]
        index = pd.Index(ids, name="ID")
        df_train, expected_fit = make_data(index)

        df_test, expected = make_df_test_and_expected(expected_fit)

        actual = (
            BaselineDetector(groupby=["ID"])
            .fit(df_train)
            .predict(df_test)["Anomaly"]
            .to_numpy()
        )

        assert_array_equal(actual, expected)

    def test_predict_groupby_str(self):
        ids = [1, 2, 3]
        index = pd.MultiIndex.from_arrays([ids], names=["ID"])
        df_train, expected_fit = make_data(index)

        df_test, expected = make_df_test_and_expected(expected_fit)

        actual = (
            BaselineDetector(groupby="ID")
            .fit(df_train)
            .predict(df_test)["Anomaly"]
            .to_numpy()
        )

        assert_array_equal(actual, expected)

    def test_predict_groupby_list(self):
        ids = [1, 2, 3]
        fraction_observed = [0.05, 0.1]

        index = pd.MultiIndex.from_product(
            [ids, fraction_observed], names=["ID", "Fraction_Observed"]
        )
        df_train, expected_fit = make_data(index)

        df_test, expected = make_df_test_and_expected(expected_fit)

        actual = (
            BaselineDetector(groupby=["ID", "Fraction_Observed"])
            .fit(df_train)
            .predict(df_test)["Anomaly"]
            .to_numpy()
        )

        assert_array_equal(actual, expected)

    def test_plot(self):
        ids = [1, 2, 3]
        index = pd.MultiIndex.from_arrays([ids], names=["ID"])
        df_train, _ = make_data(index)
        predictions = BaselineDetector(groupby="ID").fit_predict(df_train)
        ax = atd2020.detector.plot(predictions)
        self.assertIsInstance(ax, mpl.axes.Axes)


if __name__ == "__main__":
    unittest.main(
        testRunner=xmlrunner.XMLTestRunner(output="test-reports"),
        failfast=False,
        buffer=False,
        catchbreak=False,
    )
