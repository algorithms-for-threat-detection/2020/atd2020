import unittest
from pathlib import Path

import numpy as np
import pandas as pd

import atd2020


def make_data(start="2020/1/1 00:00:00"):
    n_obs = 5
    ids = np.repeat([0, 1, 2], n_obs)
    pkept = np.repeat([0.1, 0.1, 0.2], n_obs)
    timestamp_id = pd.date_range(start, periods=n_obs, freq="h").to_list()
    timestamp = np.tile(timestamp_id, 3)
    anomaly = np.random.random((len(ids),)) > 0.5

    return pd.DataFrame(
        {
            "ID": ids,
            "Fraction_Observed": pkept,
            "Timestamp": timestamp,
            "Anomaly": anomaly,
        }
    )


class TestRead(unittest.TestCase):
    def test_read_data_parquet_brotli(self):
        df = atd2020.utilities.read_data(Path("data/City1.parquet.brotli"))
        self.assertIn("Timestamp", df.columns)
        self.assertIn("TotalFlow", df.columns)
        self.assertIsInstance(df["Timestamp"].values[0], np.datetime64)

    def test_read_data_csv(self):
        df = atd2020.utilities.read_data(Path("results/City1.csv"))
        self.assertIn("Timestamp", df.columns)
        self.assertIn("Anomaly", df.columns)
        self.assertIsInstance(df["Timestamp"].values[0], np.datetime64)


class TestWrite(unittest.TestCase):
    def setUp(self):
        self.filepath = Path("results/test.csv")

    def tearDown(self):
        self.filepath.unlink(missing_ok=True)

    def test_output_csv_is_readable(self):
        df_expected = make_data()
        atd2020.utilities.to_csv(df_expected, self.filepath)
        df_actual = atd2020.utilities.read_data(self.filepath)
        pd.testing.assert_frame_equal(df_expected, df_actual)


if __name__ == "__main__":
    unittest.main(
        testRunner=xmlrunner.XMLTestRunner(output="test-reports"),
        failfast=False,
        buffer=False,
        catchbreak=False,
    )
