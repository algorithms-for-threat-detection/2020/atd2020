"""Various I/O utility functions."""

from pathlib import Path

import pandas as pd

DATE_FORMAT = "%m/%d/%Y %H:%M:%S"


def read_data(filepath, date_format=DATE_FORMAT):
    """Read parquet or csv into a dataframe with "Timestamp" as datetime.

    Parameters
    ----------
    filepath : str or Pathlib.Path
        Path to .csv or .parquet file containing "Timestamp" column.
    date_format : str
        Date format for pd.to_datetime().

    Returns
    -------
    pandas.DataFrame
    """
    filepath = Path(filepath)
    if filepath.suffix == ".csv":
        df = pd.read_csv(filepath)
    else:
        df = pd.read_parquet(filepath)
    df["Timestamp"] = pd.to_datetime(df["Timestamp"], format=date_format)
    return df


def to_csv(df, filepath):
    """Write dataframe to csv in format compatible with atd2020.metrics.

    Parameters
    ----------
    df : pandas.DataFrame

    filepath : str or Pathlib.Path
    """
    df.to_csv(filepath, index=False, date_format=DATE_FORMAT)
