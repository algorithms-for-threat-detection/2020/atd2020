"""Functions for linearly detrending data."""

import pandas as pd
import scipy


def _detrend(df, ycol="TotalFlow"):
    """Remove trend from TotalFlow for a given ID."""
    t = (df["Timestamp"] - df["Timestamp"].min()).dt.total_seconds()
    y = df[ycol]
    m, b, *_ = scipy.stats.linregress(t, y)
    return y - (m * t + b) + y.mean()


def detrend(df, inplace=False):
    """Remove trend from TotalFlow.

    Parameters
    ----------
    df : pandas.DataFrame
        df containing columns=["ID", "Timestamp", TotalFlow"].

    inplace : bool, default=False

    Returns
    -------
    df : pandas.DataFrame or None
        Copy of df with detrended flow written to column "TotalFlow" and the original
        "TotalFlow" written to column "TotalFlow_original" or None if `inplace=True`.
    """
    if not inplace:
        df = df.copy()
    tmp = df.groupby("ID").apply(_detrend)
    df["TotalFlow_detrended"] = (
        tmp.iloc[0] if isinstance(tmp, pd.DataFrame) else tmp.droplevel(0)
    )
    return df.rename(
        columns={"TotalFlow": "TotalFlow_original", "TotalFlow_detrended": "TotalFlow"},
        inplace=inplace,
    )
