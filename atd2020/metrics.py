#!/usr/bin/env python3
"""A collection of metrics for evaluating detector performance."""

import argparse
import itertools
import sys
import warnings
from pathlib import Path

import numpy as np
import pandas as pd
import seaborn as sns
import sklearn.metrics
from matplotlib import pyplot as plt

from .detector import BaselineDetector
from .detrend import detrend
from .utilities import read_data

# This is used to eat the following error:
#   lib/python3.8/runpy.py:126: RuntimeWarning: 'atd2020.metrics' found in
#   sys.modules after import of package 'atd2020', but prior to execution of
#  'atd2020.metrics'; this may result in unpredictable behaviour
#   warn(RuntimeWarning(msg))
if not sys.warnoptions:
    warnings.filterwarnings("ignore", category=RuntimeWarning, module="runpy")


METRICS_COLUMN_ORDER = [
    "True Positive",
    "False Positive",
    "True Negative",
    "False Negative",
    "Precision",
    "Recall",
    "F1",
    "Accuracy",
    "Support",
]


def metrics(df_true, df_pred, groupby=None, output_df=True, output_flat=False):
    """Compute metrics and output as either dataframe or formatted string.

    This function computes the following metrics:
        - True Positive
        - False Positive
        - True Negative
        - False Negative
        - Precision (of anomalies)
        - Recall (of anomalies)
        - F1 (of anomalies)
        - Accuracy
        - Support

    By default, metrics are computed over the whole test set. These metrics
    are labelled as "overall" in the index.

    Optionally, metrics can be computed over subsets of the dataset using the
    groupby argument. When groupby is provided, we also compute metrics for
    all groups in _register(df_true, df_pred).groupby(groupby). These metrics
    will have the same index as their corresponding group name.

    Parameters
    ----------
    df_true : pandas.DataFrame
        Ground truth dataframe containing columns "ID", "Timestamp", "Anomaly",
        and input argument groupby.

    df_pred : pandas.DataFrame
        Predictions dataframe containing columns ID, Timestamp, Anomaly.

    groupby : label, list of labels, or None, default=None
        If label or list of labels, group data over label(s) and compute
        on groups. If None, skip groupwise metrics.

    output_df : bool, default=True
        Return dataframe (True) or formatted string (False).

    output_flat : bool, default=False
        If True, flatten dataframe into single row with a column for all
        (index, metric) pairs. This is useful for storing the results of
        multiple detectors in a single dataframe.

    Returns
    -------
    pandas.DataFrame or str
        Metrics report in format determined by groupby, output_df, and
        output_flat values.
    """
    df = _register(df_true, df_pred)

    if isinstance(groupby, list) and len(groupby) > 1:
        # Treat as MultiIndex
        index = tuple("overall" for _ in groupby)
        to_index = lambda x: pd.MultiIndex.from_tuples([x])
    else:
        index = "overall"
        to_index = lambda x: pd.Index([x])

    metrics_lst = [_metrics(df, to_index(index))]
    if groupby is not None:
        metrics_group_lst = [
            _metrics(group, to_index(index)) for index, group in df.groupby(groupby)
        ]
        metrics_lst.extend(metrics_group_lst)

    df_metrics = pd.concat(metrics_lst).convert_dtypes()
    df_metrics = df_metrics[METRICS_COLUMN_ORDER]
    df_metrics.rename_axis(groupby, inplace=True)

    df_metrics = flatten(df_metrics) if output_flat else df_metrics
    return df_metrics if output_df else df_metrics.to_string()


def _register(df_true, df_pred):
    """Merge truth and predictions into format suitable for computing metrics.

    Parameters
    ----------
    df_true : pandas.DataFrame
        Ground truth dataframe, containing columns ID, Timestamp, Anomaly.

    df_pred : pandas.DataFrame
        Predictions dataframe, containing columns ID, Timestamp, Anomaly.

    Returns
    -------
    pandas.DataFrame
        Dataframe containing all fields df_true. Columns besides ID and Timestamp,
        that are common between df_true and df_pred will have suffixes "_true" and
        "_pred" appended to their column names, respectively.
    """
    ignore_cols = set(df_true.columns.to_list()) - {"ID", "Timestamp", "Anomaly"}
    if "Observed" in df_true.columns:
        df_true = df_true[df_true["Observed"]]
    df_merged = df_true.merge(
        df_pred.drop(columns=ignore_cols, errors="ignore"),
        on=("ID", "Timestamp"),
        suffixes=("_true", "_pred"),
        validate="1:1",
    )
    assert len(df_true) == len(
        df_merged
    ), f"{len(df_true)=} does not equal {len(df_merged)=}."
    return df_merged


def _metrics(df, index=[0]):
    """Compute all metrics on df and output as dict.

    Parameters
    ----------
    df : pandas.DataFrame
        A dataframe containing columns ID, Timestamp, Anomaly_true, and Anomaly_pred.
        Can be generated using register().

    index : Index or array-like, default=[0]
        Assigned as index of dataframe.

    Returns
    -------
    pandas.DataFrame
        Dataframe with index=index and columns for the following metrics:
            - True Positive
            - False Positive
            - True Negative
            - False Negative
            - Precision (of anomalies)
            - Recall (of anomalies)
            - F1 (of anomalies)
            - Accuracy
            - Support
    """
    tn, fp, fn, tp = _confusion_matrix(df).ravel()
    report = _classification_report(df, output_dict=True)

    accuracy = report["accuracy"] if "accuracy" in report else np.nan

    return pd.DataFrame(
        {
            "True Positive": tp,
            "False Positive": fp,
            "True Negative": tn,
            "False Negative": fn,
            "Precision": report["Anomaly"]["precision"],
            "Recall": report["Anomaly"]["recall"],
            "F1": report["Anomaly"]["f1-score"],
            "Accuracy": accuracy,
            "Support": report["Anomaly"]["support"],
        },
        index=index,
    )


def _classification_report(df, output_dict=False):
    """Run sklearn's classification report on a dataframe.

    Paramters
    ---------
    df : pandas.DataFrame
        A dataframe containing columns ID, Timestamp, Anomaly_true, and Anomaly_pred.
        Can be generated using register().

    output_dict : bool, default=False
        Output dict (True) or formatted string (False).

    Returns
    -------
    string / dict
        Text summary of the precision, recall, F1 score for each class.
        Dictionary returned if output_dict is True. Dictionary has the
        following structure::
            {'label 1': {'precision':0.5,
                         'recall':1.0,
                         'f1-score':0.67,
                         'support':1},
           'label 2': { ... },
            ...
            }
        The reported averages include macro average (averaging the unweighted
        mean per label), weighted average (averaging the support-weighted mean
        per label), and sample average (only for multilabel classification).
        Micro average (averaging the total true positives, false negatives and
        false positives) is only shown for multi-label or multi-class
        with a subset of classes, because it corresponds to accuracy otherwise.
        See also :func:`precision_recall_fscore_support` for more details
        on averages.
        Note that in binary classification, recall of the positive class
        is also known as "sensitivity"; recall of the negative class is
        "specificity".
    """
    return sklearn.metrics.classification_report(
        df["Anomaly_true"],
        df["Anomaly_pred"],
        output_dict=output_dict,
        labels=[False, True],
        target_names=["Normal", "Anomaly"],
        zero_division=0,
    )


def _confusion_matrix(df):
    """Run sklearn's classification report on a dataframe.

    Paramters
    ---------
    df : pandas.DataFrame
        A dataframe containing columns ID, Timestamp, Anomaly_true, and
        Anomaly_pred. Can be generated using register().

    Returns
    -------
    ndarray of shape (n_classes, n_classes)
        Confusion matrix.
    """
    return sklearn.metrics.confusion_matrix(
        df["Anomaly_true"], df["Anomaly_pred"], labels=[False, True]
    )


def flatten(df):
    """Flatten dataframe into one row with a column for all (index, metric) pairs.

    Parameters
    ----------
    df : pandas.DataFrame with non-MultiIndex index.

    Returns
    -------
    pandas.DataFrame
        Flattened dataframe containing a single row with columns for all
        (index, metric) pairs with columns.name=df.index.name.
    """
    if isinstance(df.index, pd.MultiIndex):
        raise TypeError("Flatten does not support MultiIndexes.")

    index_name = df.index.name
    if len(df) > 1:
        df = df.reset_index(drop=False).melt(id_vars=index_name, var_name="metric")
        df = (
            df.set_index(df["metric"].str.cat(df[index_name].astype(str), sep="@"))
            .drop(columns=[index_name, "metric"])
            .transpose()
            .rename_axis(columns=f"metric@{index_name}")
        )
    return df.reset_index(drop=True)


def plot(df, metric="F1", numeric_x=False, **kwargs):
    """Plot particular metric for all columns matching "metric@*".

    Parameters
    ----------
    df : pandas.DataFrame
        A flattened metrics dataframe.

    metric : str, default="F1"
        Metric to plot.

    numeric_x : bool, default=False
        If True, convert column name suffixes after @ (e.g., 0.1 in F1@0.1) to
        numeric prior to plotting. Note: this ignores "*@overall" columns.
        Otherwise, the line plot will treat suffixes as strings.

    **kwargs
        The remaining keyword arguments are passed to pandas.DataFrame.plot().

    Returns
    -------
    ax : matplotlib.axes.Axes
        For each row in the index, create a lineplot over all columns matching
        "metric@*" with the suffix of the column name (after the "@") as the
        x-axis and corresponding metric value on the y-axis. If numeric_x is
        True, we convert the column name suffix to numeric and ignore any
        columns with non-numeric suffixes (e.g., "overall").
    """
    xaxis_name = "Split" if df.columns.name is None else df.columns.name.split("@")[-1]

    metric_cols = sorted([col for col in df.columns if col.startswith(metric)])
    df_t = df[metric_cols].transpose()
    df_t[xaxis_name] = _suffix(df_t.index, to_float=numeric_x)
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=UserWarning)
        ax = df_t.set_index(xaxis_name).plot(**kwargs)
    ax.set_ylabel(f"{metric}")
    ax.set_title(f"{metric} vs. {xaxis_name}")
    return ax


def _suffix(cols, to_float=False):
    """Convert column names of the form "*@*" to numeric values.

    Namely, text after the @ will converted to a float if it is numeric.
    Otherwise, it will converted to np.nan, which will cause it to be ignored
    when plotting.

    Parameters
    ----------
    cols : list of strs
    to_float : bool, default=False
        Cast suffix as to float if True, otherwise do nothing.

    Returns
    -------
    list of cast suffixes
    """
    cast = _tofloat if to_float else lambda x: x

    return [cast(col.split("@")[-1]) for col in cols]


def _tofloat(value):
    """Attempt to convert input to float. If unsuccessful, return np.nan.

    Parameters
    ----------
    value : str

    Returns
    -------
    float or np.nan
    """
    try:
        return float(value)
    except ValueError:
        return np.nan


def _metrics_cli(args):
    """CLI for computing and printing metrics.

    Parameters
    ----------
    args : dict
        Parsed command-line arguments.
    """
    df_pred = read_data(args.pred)
    df_true = read_data(args.truth)

    metrics_ = metrics(df_true, df_pred, output_df=False, groupby=args.groupby)
    print(metrics_)
    return 0


def _baseline_metrics(data):
    # Detrend data
    data_detrended = detrend(data)

    # Generate many combinations of detector settings
    groupby = [
        ("ID", "Hour"),
    ]
    n_stds = [2, 3]
    datasets = {"raw": data, "detrended": data_detrended}
    settings = list(itertools.product(groupby, datasets.keys(), n_stds))

    # Detect anomalies using all setting combinations
    anomalies = [
        BaselineDetector(groupby=groupby).fit_predict(
            datasets[dataset_choice], n_std=n_std
        )
        for groupby, dataset_choice, n_std in settings
    ]
    settings_anomalies = list(zip(settings, anomalies))

    # Compute metrics
    df_metrics = pd.concat(
        [
            metrics(data, anom, groupby="Fraction_Observed", output_flat=True)
            for setting, anom in settings_anomalies
        ],
    ).set_index(
        pd.MultiIndex.from_tuples(settings, names=("groupby", "dataset", "n_std"))
    )

    df_metrics.index = df_metrics.index.to_flat_index()
    return df_metrics


def evaluate_submission(
    truth_path,
    pred_path,
    date_format=None,
    name="Submission",
    results_dir=Path("results"),
):
    """Write metrics .csv and plot to file.

    Parameters
    ----------
    truth_path : str or pathlib.Path
        Path to `.parquet.brotli` file containing ground truth anomaly labels.
    pred_path : str or pathlib.Path
        Path to `.csv` file containing proposed anomaly labels.
    date_format : str or None
        Date format provided to `atd2020.utilities.read_data`.
    name : str
        Name used as part of plot title and as filename prefix for output.
    results_dir : str or pathlib.Path
        Directory to output results to.

    Returns
    -------
    df_metrics : pd.DataFrame
        Metrics DataFrame for provided submission.
    df_all_metrics : pd.DataFrame
        Flattened metrics DataFrame summarizing both the baseline metrics and
        provided submission. See :func:`flatten`
    ax : plt.axes
        Metrics plot axes handle.
    """
    results_dir = Path(results_dir)

    # Read ground truth data in and drop out non-observed samples
    df_truth = read_data(truth_path)
    df_truth = df_truth[df_truth["Observed"]]

    # Read predictions in
    df_pred = read_data(pred_path, date_format=date_format)

    # Compute prediction metrics
    df_metrics = metrics(df_truth, df_pred, groupby="Fraction_Observed")
    df_metrics.to_csv(results_dir / f"{name}.csv")

    # Compute baseline metrics
    df_baseline_metrics = _baseline_metrics(df_truth)

    # Concatenate metric results into single dataframe
    df_all_metrics = pd.concat([flatten(df_metrics), df_baseline_metrics])
    new_index = [name]
    new_index.extend([f"Baseline{i}" for i in df_baseline_metrics.index])
    df_all_metrics.index = new_index

    # Compute macro average
    df_all_metrics["F1@macro_avg"] = df_all_metrics[
        [
            c
            for c in df_all_metrics.columns
            if c.startswith("F1") and not c.endswith("overall")
        ]
    ].mean(axis=1)

    # Plot F1 vs. fraction observed
    sns.set(rc={"figure.figsize": (8, 6)})
    ax = plot(df_all_metrics.iloc[[0]], numeric_x=True, metric="F1", marker="o")
    plot(
        df_all_metrics.iloc[1:],
        numeric_x=True,
        metric="F1",
        marker=".",
        linestyle="--",
        ax=ax,
    )
    plt.ylim([0, 1])
    plt.title(
        f"F1 vs Fraction_Observed (F1_Overall = {df_metrics.loc['overall']['F1']:.3f}, Macro Avg. F1 = {df_all_metrics.iloc[0]['F1@macro_avg']:.3f})"
    )
    plt.savefig(results_dir / f"{name}_f1_vs_frac_obs.png")

    return df_metrics, df_all_metrics, ax


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="atd2020.metrics", description="atd2020.metrics command-line interface."
    )
    parser.add_argument("-p", "--pred", help="path to predictions *.csv")
    parser.add_argument("-t", "--truth", help="path to truth *.parquet.brotli")
    parser.add_argument("-g", "--groupby", nargs="+", help="groupby label(s)")

    args = parser.parse_args()

    sys.exit(_metrics_cli(args))
