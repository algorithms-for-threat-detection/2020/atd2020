{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Welcome to the ATD Traffic Challenge!\n",
    "\n",
    "For the dataset provided, your challenge is to create a model to find anomalous observations in sparsely sampled traffic flow observations. Here's how we define an anomaly for the challenge:\n",
    "\n",
    "**Definition of anomaly:**\n",
    "\n",
    "For a given sensor, fit a linear trend model to its hourly flow over time. Subtract this trend from the flow, and re-add the original flow's mean. Denote this detrended flow as $d$.\n",
    "\n",
    "For a given sensor $s$, hour $h$, and weekday $w$, the ${n}^{th}$ observation of the detrended total flow $d_{shw}^{(n)}$ is anomalous if,\n",
    "\n",
    "$$\\left|d_{shw}^{(n)} - \\mu_{shw} \\right| \\geq 3\\sigma_{shw}$$\n",
    "\n",
    "\n",
    "where $\\mu_{shw}$ and $\\sigma_{shw}$ are the detrended flow sample mean and standard deviation, respectively.\n",
    "\n",
    "**Data source:**\n",
    "\n",
    "The hourly traffic flow data was obtained from the California Department of Transformation; you can download raw data and read the user's manual once you've made an account at the [Caltrans Pems website](http://pems.dot.ca.gov/).\n",
    "\n",
    "**What to expect in this Notebook...:**\n",
    "\n",
    "In this Notebook, we'll walk through how to detect anomalous observations in the complete, ground truthed City 1 dataset. Because the the anomalies are already marked in this data set, we can analyze the performance of our algorithm when we apply a baseline detector to find anomalies in sparse samples of the data. We'll read in and view the data, detrend the traffic flow values to make the data stationary, and detect anomalies using a baseline detector. Finally, we'll compute the precision, recall, and $F_{1}$ scores to analyze the quality of our predictions.\n",
    "\n",
    "**References:**\n",
    "\n",
    "* For documentation on the precision, recall, and $F_{1}$ scores calculated below, please refer to the [scikit learn documentation](https://scikit-learn.org/stable/modules/model_evaluation.html#classification-metrics) on metrics and scoring.\n",
    "* For an overview of the Jupyter Notebook Interface, please refer to the [user documentation](https://jupyter-notebook.readthedocs.io/en/stable/notebook.html#notebook-user-interface)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import itertools\n",
    "from pathlib import Path\n",
    "\n",
    "%matplotlib notebook\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import seaborn as sns\n",
    "\n",
    "import atd2020\n",
    "\n",
    "%load_ext nb_black"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Read in the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filename = Path(\"data/City1.parquet.brotli\")\n",
    "# filename = Path(\"data/City2_downsampled.parquet.brotli\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(atd2020.utilities.read_data)\n",
    "\n",
    "data = atd2020.utilities.read_data(filename)\n",
    "data.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Columns in Data:\n",
    "\n",
    "- **Timestamp:** \n",
    "    - The date and time at the beginning of the 1 hour interval, format %m/%d/%Y %H:%M:%S.\n",
    "    - datetime64.\n",
    "- **ID:**\n",
    "    - Unique station identifier. A station is a vehicle detection device implanted in the road which measures vehicle flow and occupancy data every 30 seconds every day all day. This data gets aggregated into 5-min and 1-hour aggregations. The IDs given in the dataset are not the original values; we have remapped the station IDs to make them ambiguous.\n",
    "    - int64.\n",
    "- **Observed:**\n",
    "    - The percentage of data that is actually observed, as opposed to imputed, at the 5 min aggregate level.\n",
    "    - int64.\n",
    "- **TotalFlow:**\n",
    "    - Sum of cars passing over the station in an hour (number of vehicles/hour).\n",
    "    - float64.\n",
    "- **Year:**\n",
    "    - The four digit year the observation was taken, format YYYY.\n",
    "    - int64.\n",
    "- **Month:**\n",
    "    - The month the observation was taken, format 1-12.\n",
    "    - int64.\n",
    "- **Day:**\n",
    "    - The day the observation was taken, format 1-31.\n",
    "    - int64.\n",
    "- **Hour:**\n",
    "    - The hour the observation was taken, format 0-23.\n",
    "    - int64.\n",
    "- **Weekday:**\n",
    "    - The day of the week the observation was taken, format {\"Monday\", ..., \"Sunday\"}.\n",
    "    - object.\n",
    "- **Latitude:**\n",
    "    - The normalized latitude of the sensor (values range from 0-1).\n",
    "    - float64.\n",
    "- **Longitude:**\n",
    "    - The normalized longitude of the sensor (values range from 0-1).\n",
    "    - float64.\n",
    "- **Anomaly:**\n",
    "    - Anomalies marked as True, otherwise False.\n",
    "    - Included in the complete data set (City 1) only.\n",
    "    - bool.\n",
    "- **Fraction_Observed:**\n",
    "    - If `Fraction_Observed=0.02`, that means that the data was randomly downsampled so that only 2% of the original data remains.\n",
    "    - Example: If there were 100 observations in the original dataset for each sensor, and a particular sensor has `Fraction_Observed=0.1`, then that sensor would have 10 observations with `Observed=True` and the remaining 90 observations would have `Observed=False`.\n",
    "    - This column can be helpful when measuring how detector performance varies with the amount of data available.\n",
    "    - float64.\n",
    "- **Observed:**\n",
    "    - An observation is marked `True` if it was observed by a sensor, otherwise `False`.\n",
    "    - Note: For City1, this column will be a mixture of `True` and `False` values. This is because all of the observations are included in this dataset regardless of whether they were downsampled.\n",
    "    - Note: For City2, this column will contain only `True` values. This is because all observations with `Observed=False` have already been removed from the dataset.\n",
    "    - bool.\n",
    "\n",
    "There are 500 randomly chosen station IDs in the dataframe, each with an observation at every hour over a two-year time span, with no missing data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Viewing the data\n",
    "\n",
    "By setting the index of our pandas.DataFrame to the `Timestamp` column, we are able to conveniently slice and graph the data. Here are some useful graphs for viewing the data. You can see more examples of slicing and viewing time series data [here](https://www.dataquest.io/blog/tutorial-time-series-analysis-with-pandas/).\n",
    "\n",
    "Below, we plot the `TotalFlow` of all sensors in the dataset over time. So, there are 500 markers at every timestamp."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set the index to Timestamp\n",
    "data.set_index(\"Timestamp\", drop=False, inplace=True)\n",
    "\n",
    "# Use seaborn style defaults and set the default figure size\n",
    "sns.set(rc={\"figure.figsize\": (8, 6)})\n",
    "\n",
    "# Plot the total traffic Volume 'TotalFlow' for all sensors\n",
    "_, ax = plt.subplots()\n",
    "data[\"TotalFlow\"].plot(linewidth=0.25, ax=ax)\n",
    "ax.set_ylabel(\"Traffic Volume\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the example below, we plot a small slice of the observations for the last few days in November 2016."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "_, ax = plt.subplots()\n",
    "data[\"2016-11-28\":\"2016-11-30\"][\"TotalFlow\"].plot(marker=\".\", linewidth=0.25, ax=ax)\n",
    "ax.set_xlabel(\"Timestamp (Month-Day Hour)\")\n",
    "ax.set_ylabel(\"TotalFlow (Cars/Hour)\")\n",
    "ax.set_title(\"Observations in City 1\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Grouping the dataframe:\n",
    "You can also group the dataframe into a pandas GroupBy object. You can group by one or multiple columns, where each resulting group is a dataframe. Let's group by station ID, Weekday, and Hour and take a look at the data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Group the data, each group is a dataset\n",
    "groups = data.groupby([\"ID\", \"Weekday\", \"Hour\"])\n",
    "group_lst = list(groups.groups)[:5]\n",
    "print(group_lst)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get a group by name\n",
    "sensor_id = 0\n",
    "weekday = \"Monday\"\n",
    "hour = 0\n",
    "group_name = (sensor_id, weekday, hour)\n",
    "groups.get_group(group_name).head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plotting a group and identifying anomalies in the graph:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we'll calculate the stationary mean and standard deviation for each group. A stationary time series is one where the mean and variance are not time dependant. We also calculate the number of observations per group. Lastly, we'll define an anomaly as an observations that lies three standard deviations from the mean, so we'll calculate the upper and lower boundaries for each group as well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define which group you would like to see. We chose the first group in the group_lst:\n",
    "station_weekday_hour = group_lst[0]\n",
    "print(station_weekday_hour)\n",
    "group = groups.get_group(station_weekday_hour)\n",
    "\n",
    "# Calculate the sample mean and sample standard deviation of TotalFlow for the group.\n",
    "# In addition, since we define anomalous behavior as three standard deviations away\n",
    "# from the mean, we can compute the upper and lower bounds that define anomalous\n",
    "# behavior.\n",
    "group_sample_mean = group[\"TotalFlow\"].mean()\n",
    "group_sample_std = group[\"TotalFlow\"].std()\n",
    "anomaly_lower_bound = group_sample_mean - 3 * group_sample_std\n",
    "anomaly_upper_bound = group_sample_mean + 3 * group_sample_std\n",
    "print(group_sample_mean, group_sample_std, anomaly_lower_bound, anomaly_upper_bound)\n",
    "\n",
    "# For the chosen sensor plot the Traffic Flow (cars/hour) with the mean and anomaly\n",
    "# thresholds overlaid.\n",
    "sns.set(rc={\"figure.figsize\": (8, 4)})\n",
    "_, ax = plt.subplots()\n",
    "group[\"TotalFlow\"].plot(marker=\".\", linestyle=\"none\", ax=ax)\n",
    "ax.hlines(\n",
    "    [group_sample_mean, anomaly_lower_bound, anomaly_upper_bound],\n",
    "    group.index.min(),\n",
    "    group.index.max(),\n",
    "    colors=[\"blue\", \"red\", \"red\"],\n",
    "    linestyle=\"dashed\",\n",
    ")\n",
    "\n",
    "# Add labels\n",
    "ax.set_ylabel(\"Cars/hour\")\n",
    "ax.set_title(\"Station {}, {} Hour {} Traffic Volume\".format(*station_weekday_hour))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the plot above, we have chosen one sensor. You can change the sensor being plotted in the first line of the cell block above. There are 500 sensors in the data, so you can replace the number in 'station = ID_list[0]' with any integer from 0-499.\n",
    "\n",
    "The blue dots each represent an hourly aggregated TotalFlow reading at the chosen sensor on the chosen weekday and within the chosen hour. The observations lying outside the boundaries are anomolous."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Prepare the data for anomaly detection\n",
    "\n",
    "We need to do two things to prepare the data for anomaly detection:\n",
    "\n",
    "1. Remove all observations with `Observed=False`\n",
    "    - Since we have imported `City1`, the complete dataset, we need to downsample the data by removing any observations where `Observed=False` before we run the Baseline detector. Otherwise, we would be using data that was not actually observed by the sensor.\n",
    "2. Detrend the data\n",
    "    - Recall that we defined an observation to be anomalous if its `TotalFlow` is over three standard deviations away from the **detrended** mean `TotalFlow` for the observation's sensor ID, weekday, and hour.\n",
    "    - The data was detrended prior to anomaly detection in order to account for linear growth trends. To illustrate how detrending the data will affect which observations will be labelled as anomalies, consider a toy example where a sensor observes exactly one more car each day than the last. In this case, the most extreme observations would be at the beginning and end of the time period. However, because we remove the linear trend, the detrended TotalFlow time series will be flattened into a series with a constant value, and no observations will be labelled as anomalous."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(atd2020.detrend.detrend)\n",
    "\n",
    "# Reset the index to observation number, since we previously set it to Timestamp when plotting in the above cells\n",
    "data.reset_index(drop=True, inplace=True)\n",
    "\n",
    "# If using full dataset, drop observations that were not \"keep\"\n",
    "if \"Observed\" in data.columns:\n",
    "    data = data.loc[data[\"Observed\"]].copy()\n",
    "\n",
    "# Detrend the data to make it stationary (constant mean and variance)\n",
    "data_detrended = atd2020.detrend.detrend(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Detect anomalies using the baseline detector  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(atd2020.detector.BaselineDetector)\n",
    "\n",
    "# Returns a dataframe with predicted anomalies in the 'Anomaly' column\n",
    "anomalies_baseline = atd2020.detector.BaselineDetector().fit_predict(data_detrended)\n",
    "anomalies_baseline.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Write detections to CSV file\n",
    "The output CSV file is in the format required for challenge submissions. You can find the output CSV in the `results/` directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(atd2020.utilities.to_csv)\n",
    "\n",
    "# Select 'Timestamp', 'ID', and 'Anomaly' columns and write our\n",
    "# predictions dataframe to a CSV file in the results/ directory.\n",
    "atd2020.utilities.to_csv(\n",
    "    anomalies_baseline[[\"Timestamp\", \"ID\", \"Anomaly\"]],\n",
    "    (Path(\"results\") / filename.stem).with_suffix(\".csv\"),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Compute metrics\n",
    "\n",
    "The `results/City1.csv` file contains our anomaly predictions on the downsampled data set and the 'Anomaly' column of `data/City1.parquet.brotli` contains the anomaly *truths*. We make use of [*scikit learn's* classification metrics](https://scikit-learn.org/stable/modules/model_evaluation.html#classification-metrics) to calculate the *precision*, *recall*, $F_{1}$ *score*, and *accuracy* of our prediction as follows:\n",
    "\n",
    "\n",
    "$$precision = \\frac{tp}{tp + fp},$$\n",
    "\n",
    "$$recall = \\frac{tp}{tp + fn},$$\n",
    "\n",
    "$$F_{1} = 2\\cdot \\frac{precision\\cdot recall}{precision + recall},$$\n",
    "\n",
    "$$accuracy = \\frac{tp + tn}{tp + tn + fp + fn},$$\n",
    "\n",
    "where $tp$, $fp$, $tn$, and $fn$ are defined as the number of true positives, false positives, true negatives, and false negatives, respectively. These metrics measure what fraction of our predicted anomalies were true anomalies (precision), what fraction of true anomalies did we correctly predict (recall), the harmonic mean of precision and recall ($F_{1}$ score), and the fraction of correct predictions (both positive and negative) out of all predictions made (accuracy). \n",
    "\n",
    "In the following cell, we compute the number of $tp$, $fp$, $tn$, and $fn$, as well as the precision, recall, $F_{1}$ score, and accuracy for each `Fraction_Observed` group and for the entire (*overall*) downsampled data set. Also included is the *support* for each group and for the overall data set, defined as the number of true anomalies\n",
    "\n",
    "$$support = tp + fn.$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(atd2020.metrics.metrics)\n",
    "\n",
    "assert (\n",
    "    \"Anomaly\" in data_detrended.columns\n",
    "), f\"{filename.name} does not contain truth, so we can't compute metrics.\\n\\nThis is expected behavior for City2.\"\n",
    "\n",
    "metrics_baseline = atd2020.metrics.metrics(\n",
    "    data_detrended,\n",
    "    anomalies_baseline,\n",
    "    groupby=\"Fraction_Observed\",\n",
    ")\n",
    "metrics_baseline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From this table we see that the detector's failed to detect any anomalies when `Fraction_Observed` was `0.01` or `0.02`. More generally, we see that detector performance improves as `Fraction_Observed` increases, taking on a maximum `F1` score of `0.6161` for `Fraction_Observed=0.2`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Plot the baseline detector's $F_{1}$ score vs. `Fraction_Observed` "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(atd2020.metrics.plot)\n",
    "help(atd2020.metrics.flatten)\n",
    "\n",
    "atd2020.metrics.plot(\n",
    "    atd2020.metrics.flatten(metrics_baseline),\n",
    "    metric=\"F1\",\n",
    "    numeric_x=True,\n",
    "    marker=\"x\",\n",
    "    legend=None,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can make the same conclusions from this plot as the above metrics table. However, it is perhaps easier to see the trend that `F1` increases as `Fraction_Observed` increases."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Plot forecast and anomaly predictions for several stations\n",
    "\n",
    "Another way to view the baseline anomaly detector is to zoom out of 'ID', 'Weekday', and 'Hour' groups to see what the $3\\sigma$ confidence intervals look like at the detector level."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(atd2020.detector.plot)\n",
    "\n",
    "station = 779\n",
    "atd2020.detector.plot(anomalies_baseline.groupby(\"ID\").get_group(station))\n",
    "plt.title(f\"Forecast and anomaly predictions for Station {station}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Just as before, we can zoom in on the plot to see where observations lie with respect to the confidence intervals. Those outside the shaded region are anomalies. Let's look at another sensor for good measure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "station = 513\n",
    "atd2020.detector.plot(anomalies_baseline.groupby(\"ID\").get_group(station))\n",
    "plt.title(f\"Forecast and anomaly predictions for Station {station}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Can we improve performance by using different detector settings?\n",
    "In the calculations above, we made anomaly predictions using $3\\sigma$ confidence intervals and `('ID','Hour','Weekday')` groups, which reflects how the anomaly *truths* were defined, but is this the best performing model at low downsampling rates?\n",
    "\n",
    "We will try varying several parameters:\n",
    "\n",
    "* groupby - The groups of observations over which mean and stdev are computed.\n",
    "* n_stds - Number of standard deviations away from the mean after which we consider an observation anomalous.\n",
    "* datasets - Try running on both the detrended data and raw data\n",
    "\n",
    "We'll iterate over many combinations of these parameter settings and compare their performance using metrics results."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run anomaly detection with different settings"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate many combinations of detector settings\n",
    "groupby = [\n",
    "    (\"ID\", \"Weekday\", \"Hour\"),\n",
    "    (\"ID\", \"Hour\"),\n",
    "]\n",
    "n_stds = [2, 3]\n",
    "datasets = {\"raw\": data, \"detrended\": data_detrended}\n",
    "settings = list(itertools.product(groupby, datasets.keys(), n_stds))\n",
    "\n",
    "print(\"Computing anomalies using detectors with the following detector settings:\")\n",
    "for s in settings:\n",
    "    print(\"\\t\", s)\n",
    "\n",
    "# Detect anomalies using all setting combinations\n",
    "anomalies = [\n",
    "    atd2020.detector.BaselineDetector(groupby=groupby).fit_predict(\n",
    "        datasets[dataset_choice], n_std=n_std\n",
    "    )\n",
    "    for groupby, dataset_choice, n_std in settings\n",
    "]\n",
    "settings_anomalies = list(zip(settings, anomalies))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Compute metrics for all the detectors' output"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_metrics = pd.concat(\n",
    "    [\n",
    "        atd2020.metrics.metrics(\n",
    "            data, anom, groupby=\"Fraction_Observed\", output_flat=True\n",
    "        )\n",
    "        for setting, anom in settings_anomalies\n",
    "    ],\n",
    ").set_index(pd.MultiIndex.from_tuples(settings, names=(\"groupby\", \"dataset\", \"n_std\")))\n",
    "display(df_metrics)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are 8 variations of our anomaly detector in the table above, each with metrics computed for the \"overall\" downsampled data set and for the `Fraction_Observed` groups `[0.01, 0.02, 0.05, 0.10, 0.20]`, where each group contains 100 sensors not found in any other group and the fraction refers the fraction of observations kept after downsampling.\n",
    "\n",
    "Since this table is a bit unwieldy, let's look at columns related to one metric at a time. Let's define a helper function to select the relevant columns and sort in descending order by the performance at a particicular `Fraction_Observed`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def display_metric(df, metric, at=\"overall\"):\n",
    "    cols = sorted([col for col in df.columns if col.startswith(metric)])\n",
    "    display(df[cols].sort_values(by=f\"{metric}@{at}\", ascending=False))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's start with recall, the fraction of the true anomalies detected.\n",
    "\n",
    "In the table below, we see that recall increases as `Fraction_Observed` increases for all model variations. We see that 𝜎=2 detectors recall more anomalies than 𝜎=3 detectors, but this is to be expected, as a lower 𝜎 yields tighter thresholds for what the detector considers \"normal activity\".\n",
    "\n",
    "Of note, we see the ((ID, Weekday, Hour), detrended, $\\sigma$=3) detector had a Recall<sub>1</sub>@0.01 score of 0, meaning the detector did not accurately predict any anomalies for the most sparsely sampled sensors.\n",
    "\n",
    "This was the model used to define true anomalies, so it is perhaps surprising that it performed so poorly. However, because there are so few observations when `Fraction_Observed` is low, a possible explanation is that the model did not have enough data to accurately estimate the mean and standard deviations for all (ID, Weekday, Hour) groups."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "display_metric(df_metrics, \"Recall\", at=\"overall\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now turn to precision. We find that $\\sigma = 2$ detectors are less precise than $\\sigma = 3$ detectors. This is expected as a corollary of our reasoning above-- being more willing to accept observations as anomalies increases the false positive rate and decreases precision."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "display_metric(df_metrics, \"Precision\", at=\"overall\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because precision and recall each tell only part of the story for a classifier's performance, let's look at F<sub>1</sub> score, which is the harmonic mean of precision and recall.\n",
    "\n",
    "Looking at F<sub>1</sub>, the best performing detector was ((ID, Hour), detrended, $\\sigma$=3).\n",
    "\n",
    "We see a few interesting trends:\n",
    "* detrended outperformed raw when all other settings were equal\n",
    "* $\\sigma$=3 detectors outperformed $\\sigma$=2 when all other settings were equal"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "display_metric(df_metrics, \"F1\", at=\"overall\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, we see that lowering $\\sigma$ from $3$ to $2$ yielded an improvement in performance in F<sub>1</sub>@0.01 score."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "display_metric(df_metrics, \"F1\", at=0.01)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Plot detector $F_{1}$ score vs. `Fraction_Observed`\n",
    "\n",
    "We can plot all the detectors F<sub>1</sub> scores in a single plot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sns.set(rc={\"figure.figsize\": (8, 6)})\n",
    "ax = atd2020.metrics.plot(\n",
    "    df_metrics.loc[[(\"ID\", \"Weekday\", \"Hour\")], :, :],\n",
    "    numeric_x=True,\n",
    "    metric=\"F1\",\n",
    "    marker=\"x\",\n",
    "    linestyle=\"dashed\",\n",
    ")\n",
    "ax = atd2020.metrics.plot(\n",
    "    df_metrics.loc[[(\"ID\", \"Hour\")], :, :],\n",
    "    numeric_x=True,\n",
    "    metric=\"F1\",\n",
    "    marker=\"o\",\n",
    "    linestyle=\"solid\",\n",
    "    ax=ax,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that detectors using groupby=(ID, Weekday, Hour) perform worse than detectors using groupby=(ID, Hour) for small values of `Fraction_Observed`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Enjoy!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "atd2020",
   "language": "python",
   "name": "atd2020"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
